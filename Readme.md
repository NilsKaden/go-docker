hello world webserver written in go.

Builds inside the golang docker image.
Deploys in a tiny alpine Image.

Build with
```sh
docker build -t nilsk94/alpine:v0.1 .
```
Includes K8s Configuration for exposing the Service through NodePort 30000

## How to run ##
To create deployment run from project root:

```sh
kubectl create -f k8s/deployment.yaml

kubectl create -f k8s/service.yaml
```


check the status of the created ressources via

```sh
kubetl get deployments
kubetl get services
kubetl get pods
```

Connect to the webserver through the Node's port 30000 outisde of the cluster, or port 8080 inside the cluster. 

connect from remote:
```sh
curl http://${external-ip-adress}:30000
```

connect from inside the cluster:
```sh
curl http://${CLUSTER-IP}:8080
```


### todo: CI ###
- build and tag image nilsk94/go-docker-alpine:${dynamic tag: v0.1+1}
- push to docker hub
- update the k8s cluster with new container